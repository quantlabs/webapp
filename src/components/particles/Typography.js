import styled from "styled-components";

export const Text = styled.p`
    color: ${(props) => props.theme.colors.textColor};
    font-family: ${(props) => props.theme.const.Fonts.family.main};
    font-size: ${(props) => props.theme.const.Fonts.sizes.regular};
    margin-bottom: 0px;
`;

export const TextLarge = styled(Text)`
    font-size: ${(props) => props.theme.const.Fonts.sizes.large};
`;

export const TextH1 = styled(Text)`
    font-size: ${(props) => props.theme.const.Fonts.sizes.h1};
`;

export const TextH1Bold = styled(TextH1)`
    font-family: ${(props) => props.theme.const.Fonts.family.bold};
`;

export const TextH2 = styled(Text)`
    font-size: ${(props) => props.theme.const.Fonts.sizes.h2};
`;

export const TextH2Bold = styled(TextH2)`
    font-family: ${(props) => props.theme.const.Fonts.family.bold};
`;

export const TextH3 = styled(Text)`
    font-size: ${(props) => props.theme.const.Fonts.sizes.h3};
`;

export const TextSmall = styled(Text)`
    font-size: ${(props) => props.theme.const.Fonts.sizes.small};
`;

export const TextSmallMuted = styled(TextSmall)`
    color: ${(props) => props.theme.const.Colors.darkGrayAccent};
`;

export const TextTiny = styled(Text)`
    font-size: ${(props) => props.theme.const.Fonts.sizes.tiny};
`;
