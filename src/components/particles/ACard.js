// React
import React from "react";

import { Card } from "react-bootstrap";

// Styling
import styled from "styled-components";

// Components
import { Text } from "./Typography";

const CardOpacity = styled(Card)`
    margin-top: 10px;
    &:hover {
        opacity: 0.9;
    }
`;

const CardContainer = styled(Card.Body)`
    border-radius: 5px;
    padding: 15px;
    background-color: ${(props) => {
        if (props.variant === "default") {
            return props.theme.const.Colors.blueAccent;
        } else if (props.variant === "secondary") {
            return props.theme.isDark
                ? props.theme.const.Colors.darkGrayAccent
                : props.theme.const.Colors.brightGrayAccent;
        } else if (props.variant === "success") {
            return props.theme.const.Colors.greenMessage;
        } else if (props.variant === "danger") {
            return props.theme.const.Colors.redMessage;
        } else {
            return props.theme.const.Colors.pinkAccent;
        }
    }};
`;

const CardText = styled(Card.Text)`
    color: ${(props) => {
        if (props.variant === "default") {
            return props.theme.const.Colors.white;
        } else if (props.variant === "secondary") {
            return props.theme.isDark
                ? props.theme.const.Colors.white
                : props.theme.const.Colors.black;
        } else if (props.variant === "success") {
            return props.theme.colors.bgColor;
        } else if (props.variant === "success") {
            return props.theme.colors.bgColor;
        } else {
            return props.theme.const.Colors.white;
        }
    }};
    font-size: ${(props) => props.theme.const.Fonts.sizes.cards};
`;

// card component
export default function ACard({
    onPress,
    variant = "default",
    title,
    subtitle,
    disabled = false,
    ...props
}) {
    return (
        <CardOpacity onClick={onPress} disabled={disabled}>
            <CardContainer variant={variant}>
                {title && <CardText variant={variant}>{title}</CardText>}
                {subtitle && <CardText variant={variant}>{subtitle}</CardText>}
                {props.children}
            </CardContainer>
        </CardOpacity>
    );
}
