export const StringAnswerQuestionType = {
    long: 'StringAnswerQuestion',
    short: 'string',
};
export const FloatAnswerQuestionType = {
    long: 'FloatAnswerQuestion',
    short: 'float',
};
export const ChoiceQuestionType = {
    long: 'ChoiceQuestion',
    short: 'choice',
};
export const ExamQuestionType = {
    long: 'ExamQuestion',
    short: 'exam',
};

export const questionTypesArray = [
    FloatAnswerQuestionType,
    StringAnswerQuestionType,
    ChoiceQuestionType,
    ExamQuestionType,
];
