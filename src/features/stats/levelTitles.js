// Названия уровней пользователя
export const levelTitles = [
    'Новичок',
    'Исследователь',
    'Первооткрыватель',
    'Суперзвезда',
    'Мудрец',
    'Джедай',
];
